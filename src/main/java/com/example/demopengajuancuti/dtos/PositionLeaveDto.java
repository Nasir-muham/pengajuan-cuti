package com.example.demopengajuancuti.dtos;

import java.util.Date;

public class PositionLeaveDto {
	private long positionLeaveId;
	private String positionName;
	private int leaveAllowance;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionLeaveDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PositionLeaveDto(long positionLeaveId, String positionName, int leaveAllowance, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.positionName = positionName;
		this.leaveAllowance = leaveAllowance;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public long getPositionLeaveId() {
		return positionLeaveId;
	}

	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public int getLeaveAllowance() {
		return leaveAllowance;
	}

	public void setLeaveAllowance(int leaveAllowance) {
		this.leaveAllowance = leaveAllowance;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
