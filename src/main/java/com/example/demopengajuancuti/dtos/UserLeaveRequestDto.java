package com.example.demopengajuancuti.dtos;

import java.util.Date;

import com.example.demopengajuancuti.models.BucketApproval;

public class UserLeaveRequestDto {
	private long leaveRequestId;
	private BucketApprovalDto bucketApproval;
	private String status;
	private int remainingLeave;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	
	public UserLeaveRequestDto(long leaveRequestId, BucketApprovalDto bucketApproval, String status, int remainingLeave,
			String resolverReason, String resolvedBy, Date resolvedDate, String createdBy, Date createdDate,
			String updatedBy, Date updatedDate) {
		super();
		this.leaveRequestId = leaveRequestId;
		this.bucketApproval = bucketApproval;
		this.status = status;
		this.remainingLeave = remainingLeave;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	public UserLeaveRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getLeaveRequestId() {
		return leaveRequestId;
	}
	public void setLeaveRequestId(long leaveRequestId) {
		this.leaveRequestId = leaveRequestId;
	}
	public BucketApprovalDto getBucketApproval() {
		return bucketApproval;
	}
	public void setBucketApproval(BucketApprovalDto bucketApproval) {
		this.bucketApproval = bucketApproval;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getRemainingLeave() {
		return remainingLeave;
	}
	public void setRemainingLeave(int remainingLeave) {
		this.remainingLeave = remainingLeave;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public String getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	public Date getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
