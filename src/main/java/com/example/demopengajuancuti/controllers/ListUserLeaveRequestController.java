package com.example.demopengajuancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demopengajuancuti.dtos.UserLeaveRequestDto;
import com.example.demopengajuancuti.models.UserLeaveRequest;
import com.example.demopengajuancuti.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("/api")
public class ListUserLeaveRequestController {
	@Autowired
	UserLeaveRequestRepository userLeaveRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/listRequestLeave/{id}/{perPage}/{chosen}")
	public Map<String, Object> listRequestLeave(@PathVariable(value = "id")Long userId, @PathVariable(value = "perPage")Integer perPage, @PathVariable(value = "chosen")Integer chosen){
		Map<String, Object> result = new HashMap<>();
		
		List<UserLeaveRequest> listUserLeave = userLeaveRepo.findAll();
		List<UserLeaveRequest> listUserLeaveByIdUser = getListUserLeaveByUserId(listUserLeave, userId);
		List<UserLeaveRequestDto> listUserLeaveDto = new ArrayList<>();
		
		int bantu =1;
		for(UserLeaveRequest temp : listUserLeaveByIdUser) {
			if(bantu > ((perPage*chosen)-perPage) && bantu <= (perPage * chosen)) {
				UserLeaveRequestDto userLeaveRequestDto = modelMapper.map(temp, UserLeaveRequestDto.class);
				listUserLeaveDto.add(userLeaveRequestDto);
			}
			bantu++;
		}
		
		result.put("message", "read by user id with page success");
		result.put("data", listUserLeaveDto);
		result.put("total", listUserLeaveDto.size());
		return result;
	}
	
	public List<UserLeaveRequest> getListUserLeaveByUserId(List<UserLeaveRequest> listUserLeave, Long userId){
		List<UserLeaveRequest> listUserLeaveByIdUser = new ArrayList<>();
		
		for(UserLeaveRequest temp : listUserLeave) {
			if(temp.getBucketApproval().getUser().getUserId() == userId) {
				listUserLeaveByIdUser.add(temp);
			}
		}
		return listUserLeaveByIdUser;
	}
}
