package com.example.demopengajuancuti.controllers;

import java.lang.module.ResolutionException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demopengajuancuti.dtos.BucketApprovalDto;
import com.example.demopengajuancuti.dtos.UserLeaveRequestDto;
import com.example.demopengajuancuti.exceptions.BackdateException;
import com.example.demopengajuancuti.exceptions.BucketApprovalIdException;
import com.example.demopengajuancuti.exceptions.RemainingAllawanceException;
import com.example.demopengajuancuti.exceptions.RemainingAllowanceNotEnough;
import com.example.demopengajuancuti.exceptions.ResolverException;
import com.example.demopengajuancuti.exceptions.TanggalValidasiException;
import com.example.demopengajuancuti.exceptions.WrongdateException;
import com.example.demopengajuancuti.models.BucketApproval;
import com.example.demopengajuancuti.models.PositionLeave;
import com.example.demopengajuancuti.models.User;
import com.example.demopengajuancuti.models.UserLeaveRequest;
import com.example.demopengajuancuti.repositories.BucketApprovalRepository;
import com.example.demopengajuancuti.repositories.PositionLeaveRepository;
import com.example.demopengajuancuti.repositories.UserLeaveRequestRepository;
import com.example.demopengajuancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api/userLeaveRequest")
public class RequestLeaveController {

	@Autowired
	UserLeaveRequestRepository UserLeaveRepo;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepo;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	
	@Autowired
	UserRepository userRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	// question soal nomor 2
	@PostMapping("/create")
	public Map<String, Object> createLeaveRequest(@RequestBody BucketApprovalDto body) throws ParseException{
		Map<String, Object> result = new HashMap<>();
		
		Date hariIni = getTodayDate();
		int hasil = body.getLeaveDateFrom().compareTo(body.getLeaveDateTo());
		if(body.getLeaveDateFrom().compareTo(hariIni) >= 0) {
			if(hasil <= 0) {
				BucketApproval bucketApproval = modelMapper.map(body, BucketApproval.class);
				
				int jatahCuti = getleaveAllowance(bucketApproval.getUser().getUserId());
				int jumlahCuti = getJumlahCuti(body.getLeaveDateTo(), body.getLeaveDateFrom());
				int sisaCuti = getTotalLeave(bucketApproval.getUser().getUserId(), jatahCuti);
				int ambilCuti = getCuti(sisaCuti, jumlahCuti);
				
				if (sisaCuti > 0) {
					if (ambilCuti >= 0) {
						bucketApprovalRepo.save(bucketApproval);
						UserLeaveRequest userLeaveRequest = new UserLeaveRequest();
						userLeaveRequest.setStatus("waiting");
						userLeaveRequest.setBucketApproval(bucketApproval);
						userLeaveRequest.setCreatedBy(body.getCreatedBy());
						//userLeaveRequest.setCreatedDate(body.getCreatedDate());
						userLeaveRequest.setUpdatedBy(body.getUpdatedBy());
						//userLeaveRequest.setUpdatedDate(body.getUpdatedDate());
						userLeaveRequest.setRemainingLeave(sisaCuti);
						userLeaveRequest.setRemainingLeave(sisaCuti);
						UserLeaveRepo.save(userLeaveRequest);
						result.put("Meessage","Permohonan anda sedang diproses");
					}else throw new RemainingAllowanceNotEnough("Jatah cuti", body.getLeaveDateFrom(), body.getLeaveDateTo(), jumlahCuti, sisaCuti);
				} else throw new RemainingAllawanceException("Jatah Cuti");
			}else throw new WrongdateException();
		}else throw new BackdateException();
		
		return result;
	}	
	
	// resolved leaved question soal nomor 4
	
	@PostMapping("/resolveRequest")
	public Map<String, Object> resolveRequest(@RequestBody UserLeaveRequestDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<UserLeaveRequest> listUserLeave = UserLeaveRepo.findAll();
		List<User> listUser = userRepo.findAll(); 
		
		BucketApproval bucketApproval = bucketApprovalRepo.findById(body.getBucketApproval().getBucketApprovalId())
				.orElseThrow(()-> new BucketApprovalIdException(body.getBucketApproval().getBucketApprovalId()));
		
		UserLeaveRequest userLeaveRequest = getLeaveRequestByBucketId(body.getBucketApproval().getBucketApprovalId(), listUserLeave);
		List<UserLeaveRequestDto> test = new ArrayList<>(); 
		//UserLeaveRequestDto test = modelMapper.map(userLeaveRequest, UserLeaveRequestDto.class);
		
		for(UserLeaveRequest temp:listUserLeave) {
			UserLeaveRequestDto coba = modelMapper.map(temp, UserLeaveRequestDto.class);
			test.add(coba);
		}
		boolean isValid = validateDate(body.getResolvedDate(), userLeaveRequest.getBucketApproval().getLeaveDateFrom());
		
		
		if(isValid) {
			String positionUser = getUserPosition(userLeaveRequest);
			String positionResolver = getPositionResolver(body.getResolvedBy(), listUser);
			boolean isResolved = validateResolvedBy(positionUser, positionResolver, positionUser, userLeaveRequest, body.getResolvedBy());
			if(isResolved) {
				int jumlahCuti = getJumlahCuti(userLeaveRequest.getBucketApproval().getLeaveDateTo(), userLeaveRequest.getBucketApproval().getLeaveDateFrom());
				int jatahCuti = getleaveAllowance(userLeaveRequest.getBucketApproval().getUser().getUserId());
				int sisaCuti = getTotalLeave(userLeaveRequest.getBucketApproval().getUser().getUserId(), jatahCuti); //
				if(body.getStatus().toLowerCase().contains("approve")) {
					if((sisaCuti-jumlahCuti) > 0) {
						userLeaveRequest.setRemainingLeave(sisaCuti-jumlahCuti);
					}else throw new RemainingAllowanceNotEnough("Jatah cuti", userLeaveRequest.getBucketApproval().getLeaveDateFrom(), userLeaveRequest.getBucketApproval().getLeaveDateTo(), jumlahCuti, sisaCuti);
				}else userLeaveRequest.setRemainingLeave(sisaCuti);
				userLeaveRequest.setResolvedBy(body.getResolvedBy());
				userLeaveRequest.setResolvedDate(body.getResolvedDate());
				userLeaveRequest.setResolverReason(body.getResolverReason());
				userLeaveRequest.setStatus(body.getStatus());
				userLeaveRequest.setCreatedBy(body.getCreatedBy());
				//userLeaveRequest.setCreatedDate(body.getCreatedDate());
				userLeaveRequest.setUpdatedBy(body.getUpdatedBy());
				//userLeaveRequest.setUpdatedDate(body.getUpdatedDate());
				UserLeaveRepo.save(userLeaveRequest);
				result.put("Message", "Permohonan dengan Id "+body.getBucketApproval().getBucketApprovalId()+" telah berhasil diputuskan");
			}else throw new ResolverException(body.getResolvedBy());
		}else throw new TanggalValidasiException();
		return result;
	}
	
	
	// mengambil tanggal hari ini
	public Date getTodayDate() throws ParseException {
		Date date = new Date();
		String tanggal = dateFormat.format(date);
		return dateFormat.parse(tanggal);
	}
	
	// menghitung jumlah cuti
	public int getJumlahCuti(Date to, Date from) {
		int hasil = (int) (to.getTime() - from.getTime());
		return ((hasil/(1000*60*60*24))+1);	
	}
	
	// mencari jatah cuti dalam setahun jatah
	public int getleaveAllowance(Long userId) {
		int jatahCuti=0;
		List<PositionLeave> listPositionLeave = positionLeaveRepo.findAll();
		User user = userRepo.findById(userId).get();
		
		for(PositionLeave temp : listPositionLeave) {
			if(temp.getPositionName().equalsIgnoreCase(user.getPosition().getPositionName())) {
				jatahCuti = temp.getLeaveAllowance();
			}
		}
		return jatahCuti;
	}
	
	// menghitung total cuti yang telah di aprove
	public int getTotalLeave(Long userId, int jatahCuti) {
		int tutalLeave = 0;
		List<BucketApproval> listBucketApproval = bucketApprovalRepo.findAll();
		for(BucketApproval temp : listBucketApproval) {
			if(temp.getUser().getUserId() == userId) {
				for(UserLeaveRequest tempLeave : temp.getUserLeaveRequests()) {
					if(tempLeave.getStatus().toLowerCase().contains("approve")) {
						tutalLeave += getJumlahCuti(temp.getLeaveDateTo(), temp.getLeaveDateFrom());
					}
				}
			}
		}
		
		return jatahCuti - tutalLeave;
	}
	
	// menghitung cuti yang diambil untuk memastikan bisa cuti apa tidak
	public int getCuti(int sisaCuti, int jumlahCuti) {
		return sisaCuti-jumlahCuti;
	}
	
	// validasi tanggal keputusan cuti
	public boolean validateDate(Date resolvedDate, Date leaveDateFrom) {
		boolean isValid = false;
		if(resolvedDate.compareTo(leaveDateFrom) <= 0) {
			isValid = true;
		}
		return isValid;
	}
	
	// validasi pemberi ijin cuti
	public boolean validateResolvedBy(String resolvedBy, String positionResolver,String positionUser, UserLeaveRequest userLeaveRequest, String resolverName ) {
		boolean isValid = false;
		
		if(positionUser.equalsIgnoreCase("Employee") && positionResolver.equalsIgnoreCase("Supervisor")) {
			isValid = true;
		}
		else if(positionUser.equalsIgnoreCase("Supervisor") && positionResolver.equalsIgnoreCase("Supervisor") && ! userLeaveRequest.getBucketApproval().getUser().getUserName().equalsIgnoreCase(resolverName)) {
			isValid = true ;
		}
		else if(positionUser.equalsIgnoreCase("Staff") && positionResolver.equalsIgnoreCase("Staff")) {
			isValid = true ;
		}
		return isValid;
	}
	
	// mengambil posisi pemberi ijin
	public String getPositionResolver(String resolverName, List<User> listUser) {
		String positionResolver = null;
		
		for(User user:listUser) {
			if(user.getUserName().equalsIgnoreCase(resolverName)) {
				positionResolver = user.getPosition().getPositionName();
			}
		}
		return positionResolver;
	}
	
	// mengambil posisi yang mau cuti
	public String getUserPosition(UserLeaveRequest userLeaveRequest) {
		return userLeaveRequest.getBucketApproval().getUser().getPosition().getPositionName();
	}
	
	
	// mengambil userleaverequest berdasarkan bucketId
	public UserLeaveRequest getLeaveRequestByBucketId(Long bucketId, List<UserLeaveRequest> listUserLeave) {
		UserLeaveRequest userLeaveRequest = new UserLeaveRequest();
		
		for(UserLeaveRequest temp : listUserLeave) {
			if(temp.getBucketApproval().getBucketApprovalId() == bucketId) {
				userLeaveRequest = temp;
			}
		}
		return userLeaveRequest;
	}
	
}
