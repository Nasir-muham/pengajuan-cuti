package com.example.demopengajuancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demopengajuancuti.dtos.PositionLeaveDto;
import com.example.demopengajuancuti.exceptions.ResourceNotFoundException;
import com.example.demopengajuancuti.models.PositionLeave;
import com.example.demopengajuancuti.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionLeave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@RequestBody PositionLeaveDto body){
		Map<String, Object> result = new HashMap<>();
		
		PositionLeave positionLeave = modelMapper.map(body, PositionLeave.class);
		positionLeaveRepo.save(positionLeave);
		body.setPositionLeaveId(positionLeave.getPositionLeaveId());
		
		result.put("Message", "Create Succes");
		result.put("Data", body);
		return result;
	}
	
	// get all
	@GetMapping("/getAll")
	public Map<String, Object> getAllPositionLeave(){
		Map<String, Object> result = new HashMap<>();
		
		List<PositionLeave> listPositionLeaven = positionLeaveRepo.findAll();
		List<PositionLeaveDto> listPositionLeaveDto = new ArrayList<>();
		
		for(PositionLeave temp:listPositionLeaven) {
			PositionLeaveDto positionLeaveDto = modelMapper.map(temp, PositionLeaveDto.class);
			listPositionLeaveDto.add(positionLeaveDto);
		}
		
		result.put("Message", "Get All Succes");
		result.put("Data", listPositionLeaveDto);
		result.put("Total", listPositionLeaveDto.size());
		
		return result;
	}
	
	// get single
	@GetMapping("/getsingle/{id}")
	public Map<String, Object> findByIdPositionLeave(@PathVariable(value = "id")Long positionLeaveId){
		Map<String, Object> result = new HashMap<>();
		
		PositionLeave positionLeave = positionLeaveRepo.findById(positionLeaveId)
				.orElseThrow(()-> new ResourceNotFoundException("PosditionLeave", "id", positionLeaveId));
		PositionLeaveDto positionLeaveDto = modelMapper.map(positionLeave, PositionLeaveDto.class);
		
		result.put("Message", "find by id Succes");
		result.put("Data", positionLeaveDto);
		return result;
		
	}
	
	// update
	@PutMapping("update/{id}")
	public Map<String, Object> updatePositionLeave(@PathVariable(value = "id")Long positionLeaveId, @RequestBody PositionLeaveDto body){
		Map<String, Object> result = new HashMap<>();
		
		PositionLeave positionLeave = positionLeaveRepo.findById(positionLeaveId)
				.orElseThrow(()-> new ResourceNotFoundException("PosditionLeave", "id", positionLeaveId));
		body.setPositionLeaveId(positionLeaveId);
		positionLeave = modelMapper.map(body, PositionLeave.class);

		positionLeaveRepo.save(positionLeave);
		
		result.put("Message", "update Succes");
		result.put("Data", body);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePendapatan(@PathVariable(value = "id")Long positionLeaveId){
Map<String, Object> result = new HashMap<>();
		
		PositionLeave positionLeave = positionLeaveRepo.findById(positionLeaveId)
				.orElseThrow(()-> new ResourceNotFoundException("PosditionLeave", "id", positionLeaveId));
		
		PositionLeaveDto positionLeaveDto = modelMapper.map(positionLeave, PositionLeaveDto.class);
		positionLeaveRepo.delete(positionLeave);
		
		result.put("Message", "delete by id Succes");
		result.put("Data", positionLeaveDto);
		return result;
	}
}
