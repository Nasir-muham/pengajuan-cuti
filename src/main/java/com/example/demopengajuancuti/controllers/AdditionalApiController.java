package com.example.demopengajuancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demopengajuancuti.dtos.UserLeaveRequestDto;
import com.example.demopengajuancuti.models.UserLeaveRequest;
import com.example.demopengajuancuti.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("/api")
public class AdditionalApiController {

	@Autowired
	UserLeaveRequestRepository userLeaveRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/listRequest/{status}")
	public Map<String, Object> listRequestByStatus(@PathVariable(value = "status")String status){
		Map<String, Object> result = new HashMap<>();
		
		List<UserLeaveRequest> listLeave = userLeaveRepo.findAll();
		List<UserLeaveRequest> listLeaveByStatus = getUserLeaveByStatus(listLeave, status);
		List<UserLeaveRequestDto> listLeaveByStatusDto = new ArrayList<>(); 
		
		for(UserLeaveRequest temp : listLeaveByStatus) {
			UserLeaveRequestDto userLeaveDto = modelMapper.map(temp, UserLeaveRequestDto.class);
			listLeaveByStatusDto.add(userLeaveDto);
		}
		result.put("Message", "get by status success");
		result.put("Data", listLeaveByStatusDto);
		result.put("Total", listLeaveByStatusDto.size());
		return result;
	}
	
	public List<UserLeaveRequest> getUserLeaveByStatus(List<UserLeaveRequest> listLeave, String status){
		List<UserLeaveRequest> listLeaveByStatus = new ArrayList<>();
		
		for(UserLeaveRequest temp : listLeave) {
			if(temp.getStatus().toLowerCase().contains(status)) {
				listLeaveByStatus.add(temp);
			}
		}
		return listLeaveByStatus;
	}
}
