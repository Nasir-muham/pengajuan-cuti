package com.example.demopengajuancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demopengajuancuti.dtos.PositionDto;
import com.example.demopengajuancuti.exceptions.ResourceNotFoundException;
import com.example.demopengajuancuti.models.Position;
import com.example.demopengajuancuti.repositories.PositionRepository;

@RestController
@RequestMapping("api/position")
public class PositionController {
	@Autowired
	PositionRepository positionRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// create
	@PostMapping("/create")
	public Map<String, Object> createPosition(@RequestBody PositionDto body){
		Map<String, Object> result = new HashMap<>();
		
		Position position = modelMapper.map(body, Position.class);
		positionRepo.save(position);
		body.setPositionId(position.getPositionId());
		
		result.put("Message", "Create Succes");
		result.put("Data", body);
		return result;
	}
	
	// get all
	@GetMapping("/readAll")
	public Map<String, Object> readAllPosition(){
		Map<String, Object> result = new HashMap<>();
		
		List<Position> listPosition = positionRepo.findAll();
		List<PositionDto> listPositionDto = new ArrayList<>();
		
		for(Position temp:listPosition) {
			PositionDto positionDto = modelMapper.map(temp, PositionDto.class);
			listPositionDto.add(positionDto);
		}
		
		result.put("Message", "Get All Succes");
		result.put("Data", listPositionDto);
		result.put("Total", listPositionDto.size());
		
		return result;
	}
	
	// single by id
	@GetMapping("/readSingle/{id}")
	public Map<String, Object> readSinglePosition(@PathVariable(value = "id")Long positionId){
		Map<String, Object> result = new HashMap<>();
		
		Position position = positionRepo.findById(positionId)
				.orElseThrow(()-> new ResourceNotFoundException("Position", "id", positionId));
		PositionDto positionDto = modelMapper.map(position, PositionDto.class);
		
		result.put("Message", "find by id Succes");
		result.put("Data", positionDto);
		return result;
	}
	
	// update
	@PutMapping("update/{id}")
	public Map<String, Object> updatePosition(@PathVariable(value = "id")Long positionId,@RequestBody PositionDto body){
		Map<String, Object> result = new HashMap<>();
		Position position = positionRepo.findById(positionId)
				.orElseThrow(()-> new ResourceNotFoundException("Position", "id", positionId));
		
		body.setPositionId(positionId);
		position = modelMapper.map(body, Position.class);
		
		positionRepo.save(position);
		
		result.put("Message", "update Succes");
		result.put("Data", body);
		return result;
	}
	
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePosition(@PathVariable(value = "id")Long positionId){
		Map<String, Object> result = new HashMap<>();
		Position position = positionRepo.findById(positionId)
				.orElseThrow(()-> new ResourceNotFoundException("Position", "id", positionId));
		
		PositionDto positionDto = modelMapper.map(position, PositionDto.class);
		positionRepo.delete(position);
		
		result.put("Message", "delete id Succes");
		result.put("Data", positionDto);
		return result;
	}
}
