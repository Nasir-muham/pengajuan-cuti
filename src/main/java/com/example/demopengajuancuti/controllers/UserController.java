package com.example.demopengajuancuti.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demopengajuancuti.dtos.UserDto;
import com.example.demopengajuancuti.exceptions.ResourceNotFoundException;
import com.example.demopengajuancuti.models.User;
import com.example.demopengajuancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	UserRepository userRepo;
	ModelMapper modelMapper = new ModelMapper();
	
	// create
	@PostMapping("/create")
	public Map<String, Object> createUser(@RequestBody UserDto body){
		Map<String, Object> result = new HashMap<>();
		
		User user = modelMapper.map(body, User.class);
		userRepo.save(user);
		body.setUserId(user.getUserId());
		
		result.put("Message", "Create Succes");
		result.put("Data", body);
		return result;
	}
	
	
	@GetMapping("/getAll")
	public Map<String, Object> getAllUser(){
		Map<String, Object> result = new HashMap<>();
		
		List<User> listUser = userRepo.findAll();
		List<UserDto> listUserDto = new ArrayList<>();
		
		for(User temp:listUser) {
			UserDto userDto = modelMapper.map(temp, UserDto.class);
			listUserDto.add(userDto);
		}
		
		result.put("Message", "Get All Succes");
		result.put("Data", listUserDto);
		result.put("Total", listUserDto.size());
		
		return result;
	}
	
	@GetMapping("/getsingle/{id}")
	public Map<String, Object> findByIdUser(@PathVariable(value = "id")Long userId){
		Map<String, Object> result = new HashMap<>();
		
		User user = userRepo.findById(userId)
				.orElseThrow(()-> new ResourceNotFoundException("User", "id", userId));
				
		UserDto userDto = modelMapper.map(user, UserDto.class);
		
		result.put("Message", "find by id Succes");
		result.put("Data", userDto);
		return result;
		
	}
	
	@PutMapping("update/{id}")
	public Map<String, Object> updateUser(@PathVariable(value = "id")Long userId, @RequestBody UserDto body){
		Map<String, Object> result = new HashMap<>();
		User user = userRepo.findById(userId)
				.orElseThrow(()-> new ResourceNotFoundException("User", "id", userId));
		
		body.setUserId(userId);
		user = modelMapper.map(body, User.class);
		
		userRepo.save(user);
		
		result.put("Message", "update Succes");
		result.put("Data", body);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePendapatan(@PathVariable(value = "id")Long userId){
		Map<String, Object> result = new HashMap<>();
		User user = userRepo.findById(userId)
				.orElseThrow(()-> new ResourceNotFoundException("User", "id", userId));
		
		UserDto userDto = modelMapper.map(user, UserDto.class);
		userRepo.delete(user);
		
		result.put("Message", "find by id Succes");
		result.put("Data", userDto);
		return result;
	}

	
}
