package com.example.demopengajuancuti.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class ResolverException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3176004986055304471L;
	private String resourceName;

    public ResolverException( String resourceName) {
        super(String.format("%s cannot be resolved ", resourceName));
        this.resourceName = resourceName;
    }

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

}
