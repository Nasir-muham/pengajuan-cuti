package com.example.demopengajuancuti.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class RemainingAllawanceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2929158849206545695L;

	private String resourceName;

    public RemainingAllawanceException( String resourceName) {
        super(String.format("mohon maaf %s anda telah habis", resourceName));
        this.resourceName = resourceName;
    }
	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	
}
