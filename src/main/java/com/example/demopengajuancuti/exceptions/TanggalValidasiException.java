package com.example.demopengajuancuti.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS)
public class TanggalValidasiException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7447855502228279748L;

	public TanggalValidasiException() {
        super(String.format("Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti"));
    }
}
