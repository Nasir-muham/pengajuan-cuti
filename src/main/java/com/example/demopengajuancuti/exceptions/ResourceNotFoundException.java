package com.example.demopengajuancuti.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3959874883408084036L;
	
	private String resourceName;
    private String fieldName1;
    private String fieldName2;
    private Object fieldValue1;
    private Object fieldValue2;

    public ResourceNotFoundException( String resourceName, String fieldName1, Object fieldValue1) {
        super(String.format("%s not found with %s : '%s'", resourceName, fieldName1, fieldValue1));
        this.resourceName = resourceName;
        this.fieldName1 = fieldName1;
        this.fieldValue1 = fieldValue1;
    }
    
    public ResourceNotFoundException( String resourceName, String fieldName1, Object fieldValue1, String fieldName2, Object fieldValue2) {
    	super(String.format("%s not found with %s : '%s',and %s : '%s'", resourceName, fieldName1, fieldValue1, fieldName2,fieldValue2));
    	this.resourceName = resourceName;
    	this.fieldName1 = fieldName1;
    	this.fieldValue1 = fieldValue1;
    }

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getFieldName1() {
		return fieldName1;
	}

	public void setFieldName1(String fieldName1) {
		this.fieldName1 = fieldName1;
	}

	public String getFieldName2() {
		return fieldName2;
	}

	public void setFieldName2(String fieldName2) {
		this.fieldName2 = fieldName2;
	}

	public Object getFieldValue1() {
		return fieldValue1;
	}

	public void setFieldValue1(Object fieldValue1) {
		this.fieldValue1 = fieldValue1;
	}

	public Object getFieldValue2() {
		return fieldValue2;
	}

	public void setFieldValue2(Object fieldValue2) {
		this.fieldValue2 = fieldValue2;
	}
}