package com.example.demopengajuancuti.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BucketApprovalIdException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4543357736673250641L;
	private Object fieldName;
	
	public BucketApprovalIdException( Object fieldName) {
        super(String.format("Permohonan dengan ID %s tidak ditemukan", fieldName));
        this.fieldName = fieldName;
    }

	public Object getFieldName() {
		return fieldName;
	}

	public void setFieldName(Object fieldName) {
		this.fieldName = fieldName;
	}
    
}
