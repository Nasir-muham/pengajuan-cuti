package com.example.demopengajuancuti.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE)
public class RemainingAllowanceNotEnough extends RuntimeException{

	/**
	 * 
	 */
	private String resourceName;
    private String fieldName1;
    private String fieldName2;
    private Object fieldValue1;
    private Object fieldValue2;
    private Object fieldValue3;
    private Object fieldValue4;

    public RemainingAllowanceNotEnough( String resourceName, Object fieldValue1, Object fieldValue2,Object fieldValue3,Object fieldValue4) {
        super(String.format("Mohon maaf, %s anda tidak cukup digunakan dari tanggal %s sampai %s(%s) jatah cuti anda "
        		+ "yang tersisa adalah %s", resourceName, fieldValue1, fieldValue2, fieldValue3, fieldValue4));
        this.resourceName = resourceName;
        this.fieldValue1 = fieldValue1;
        this.fieldValue1 = fieldValue2;
        this.fieldValue1 = fieldValue3;
        this.fieldValue1 = fieldValue4;
    }
    
    public RemainingAllowanceNotEnough( String resourceName, String fieldName1, Object fieldValue1, String fieldName2, Object fieldValue2) {
    	super(String.format("%s not found with %s : '%s',and %s : '%s'", resourceName, fieldName1, fieldValue1, fieldName2,fieldValue2));
    	this.resourceName = resourceName;
    	this.fieldName1 = fieldName1;
    	this.fieldValue1 = fieldValue1;
    }

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getFieldName1() {
		return fieldName1;
	}

	public void setFieldName1(String fieldName1) {
		this.fieldName1 = fieldName1;
	}

	public String getFieldName2() {
		return fieldName2;
	}

	public void setFieldName2(String fieldName2) {
		this.fieldName2 = fieldName2;
	}

	public Object getFieldValue1() {
		return fieldValue1;
	}

	public void setFieldValue1(Object fieldValue1) {
		this.fieldValue1 = fieldValue1;
	}

	public Object getFieldValue2() {
		return fieldValue2;
	}

	public void setFieldValue2(Object fieldValue2) {
		this.fieldValue2 = fieldValue2;
	}

	public Object getFieldValue3() {
		return fieldValue3;
	}

	public void setFieldValue3(Object fieldValue3) {
		this.fieldValue3 = fieldValue3;
	}

	public Object getFieldValue4() {
		return fieldValue4;
	}

	public void setFieldValue4(Object fieldValue4) {
		this.fieldValue4 = fieldValue4;
	}
}
