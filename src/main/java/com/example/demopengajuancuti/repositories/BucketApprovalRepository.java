package com.example.demopengajuancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demopengajuancuti.models.BucketApproval;
@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long>{

}
