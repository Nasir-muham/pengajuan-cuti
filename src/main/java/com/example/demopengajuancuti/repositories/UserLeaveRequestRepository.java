package com.example.demopengajuancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demopengajuancuti.models.UserLeaveRequest;
@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long> {

}
